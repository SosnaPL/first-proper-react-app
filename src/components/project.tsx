import React from 'react';
import { Card } from 'react-bootstrap';

interface ProjectProps {
  title: string;
  description: string;
  repository?: string;
}

export default class Project extends React.Component<{ project: ProjectProps }> {
  public render(): JSX.Element {
    const {
      title, description, repository
    } = this.props.project;
    return (
      <Card className="shadow p-2 rounded vw-100">
        {
          repository
            ?
            <a className="text-decoration-none" href={repository} target="_blank">
              <Card.Title>{title}</Card.Title>
            </a>
            :
            <Card.Title style={{ color: "#00e4fd" }}>{title}</Card.Title>
        }
        <Card.Text>{description}</Card.Text>
      </Card>
    );
  }
}
