import React from 'react';
import { Card } from 'react-bootstrap';

interface ContactProps {
  title: string;
  type: string;
}

export default class Contact extends React.Component<{ contact: ContactProps }> {
  public render(): JSX.Element {
    const {
      title, type
    } = this.props.contact;
    return (
      <Card className="shadow p-2 rounded vw-100">
        <a className="text-decoration-none" href={"mailto:" + title}>
          <Card.Title>{title}</Card.Title>
        </a>
        <Card.Text>{type}</Card.Text>
      </Card>
    );
  }
}
