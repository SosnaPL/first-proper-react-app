import React from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export class Navbar extends React.Component<{ active: string }> {
  public render(): JSX.Element {
    return (
      <div className="navbar_container">
        <Link to="/">
          <Button variant="outline-info" style={{
            backgroundColor: this.props.active == "home" ? "#17A2B8" : "",
            color: this.props.active == "home" ? "#FFFFFF" : ""
          }}>
            Home
          </Button>
        </Link>
        <Link to="/projects">
          <Button variant="outline-info" style={{
            backgroundColor: this.props.active == "projects" ? "#17A2B8" : "",
            color: this.props.active == "projects" ? "#FFFFFF" : ""
          }}>
            Projects
          </Button>
        </Link>
        <Link to="/private">
          <Button variant="outline-info" style={{
            backgroundColor: this.props.active == "private_projects" ? "#17A2B8" : "",
            color: this.props.active == "private_projects" ? "#FFFFFF" : ""
          }}>
            Private
          </Button>
        </Link>
        <Link to="/contact">
          <Button variant="outline-info" style={{
            backgroundColor: this.props.active == "contact" ? "#17A2B8" : "",
            color: this.props.active == "contact" ? "#FFFFFF" : ""
          }}>
            Contact
          </Button>
        </Link>
      </div>
    );
  }
}