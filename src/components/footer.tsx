import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLinkedin, faGithub, faGitlab } from '@fortawesome/free-brands-svg-icons';

export class Footer extends React.Component {
  public render(): JSX.Element {
    return (
      <Row>
        <Col className="d-flex justify-content-center">
          <a href="https://www.linkedin.com/in/jakub-sosi%C5%84ski-954a7a19b/" target="_blank">
            <FontAwesomeIcon size="3x" icon={faLinkedin} />
          </a>
          <a href="https://github.com/SosnaPL" target="_blank">
            <FontAwesomeIcon size="3x" icon={faGithub} />
          </a>
          <a href="https://gitlab.com/SosnaPL" target="_blank">
            <FontAwesomeIcon size="3x" icon={faGitlab} />
          </a>
        </Col>
      </Row>
    );
  }
}