import React from 'react';
import { Col, Row, Container } from 'react-bootstrap';
import { HashRouter, Route, Switch } from 'react-router-dom';
import ParticlesRenderer from './components/particles_render';

const Home = React.lazy(() => import('./pages/Home'));
const Projects = React.lazy(() => import('./pages/Projects'));
const Contacts = React.lazy(() => import('./pages/Contacts'));
const PrivateProjects = React.lazy(() => import('./pages/Private'));

export default class App extends React.Component {
  public render(): JSX.Element {
    return (
      <Container className="app_container">
        <ParticlesRenderer />
        <Row>
          <Col className="mx-auto col-md-8">
            <Container className="p-3 rounded content_container">
              <HashRouter>
                <React.Suspense
                  fallback={(
                    <div className="d-flex justify-content-center">
                      <div className="spinner-border" role="projects_load" />
                    </div>
                  )}
                >
                  <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/projects" component={Projects} />
                    <Route path="/private" component={PrivateProjects} />
                    <Route path="/contact" component={Contacts} />
                    <Route component={Home} />
                  </Switch>
                </React.Suspense>
              </HashRouter>
            </Container>
          </Col>
        </Row>
      </Container>
    );
  }
}