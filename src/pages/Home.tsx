import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { Navbar } from '../components/navbar';
import { Footer } from '../components/footer';

export default class Home extends React.Component {
  public render(): JSX.Element {
    return (
      <>
        <Navbar active="home" />
        <Row>
          <Col className="d-flex justify-content-center">
            <h2>Home</h2>
          </Col>
        </Row>
        <Row>
          <Col className="d-flex justify-content-center text-center">
            <h3>Hi, I'm Jacob, a 21 years old Junior Developer.</h3>
          </Col>
        </Row>
        <Row>
          <Col className="d-flex justify-content-center text-center">
            <h4>Most of the projects that you will find on this website are my simple public applications/websites that I've made in the past or I'm still working on. More informations about my private projects will be added in the future.</h4>
          </Col>
        </Row>
        <Footer />
      </>
    );
  }
}
