import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Navbar } from '../components/navbar';
import Contact from '../components/contact';
import { Footer } from '../components/footer';

const contacts = [
  {
    title: 'jsosinski99@gmail.com',
    type: 'Email'
  }
]

export default class Contacts extends React.Component {
  public render(): JSX.Element {
    return (
      <>
        <Navbar active="contact" />
        <Row>
          <Col className="d-flex justify-content-center">
            <h2>Contact</h2>
          </Col>
        </Row>
        <Row>
          <Col>
            <Container>
              {contacts.map((contact) => (
                <Row key={contact.title}>
                  <Col className="d-flex justify-content-start pb-3">
                    <Contact contact={contact} />
                  </Col>
                </Row>
              ))}
            </Container>
          </Col>
        </Row>
        <Footer />
      </>
    );
  }
}