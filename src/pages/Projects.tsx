import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Navbar } from '../components/navbar';
import Project from '../components/project';
import { Footer } from '../components/footer';

const projects = [
  {
    title: "Genshin Apps",
    description: "Current main project based on react/typescript/webpack. Damage calculator for genshin impact with artiffacts simulator.",
    repository: "https://gitlab.com/SosnaPL/genshinaps"
  },
  {
    title: "The Widlarz Group Chat",
    description: "Recruitment assignment that focused mainly on how well I can adapt to new technologies and libraries (based on mockup, completed in 3 days).",
    repository: "https://github.com/SosnaPL/twg-chat"
  },
  {
    title: "Export Modal",
    description: "Export Modal used to export reports from a specific time period (recruitment task). ",
    repository: "https://github.com/SosnaPL/export-modal"
  },
  {
    title: "MyMusic Form",
    description: "Contractor form with validations (recruitment task). "
  },
  {
    title: "Coronavirus-Stats",
    description: "Coronavirus statistics.",
    repository: "https://sosnacovidstats.netlify.app/"
  },
  {
    title: "Chess VR",
    description: "Chess than can be played on VR and web",
    repository: "https://gitlab.com/SosnaPL/chessvr"
  },
  {
    title: 'MSTAG',
    description: 'Abandoned project with self made notifications/chat/party/friends system using React and Django.',
    repository: 'https://github.com/SosnaPL/MSTAG-Front-End'
  },
  {
    title: 'AimTraining',
    description: 'Simple game that helps you train your reflex.',
    repository: 'https://github.com/SosnaPL/AimTraining',
  },
  {
    title: 'Codewars-Kata-Solutions',
    description: 'This repository includes kata that I have completed using javascript/python.',
    repository: 'https://github.com/SosnaPL/Codewars-Kata-Solutions',
  },
  {
    title: 'Coub-Video-Creator',
    description: 'Automatic coub video creator with usage of coub api and youtubedl.',
    repository: 'https://github.com/SosnaPL/Coub-Video-Creator',
  },
  {
    title: 'Python-Macros',
    description: 'Some of my python macros.',
    repository: 'https://github.com/SosnaPL/Python-Macros',
  },
  {
    title: 'AutoHotkey-Macros',
    description: 'Some of my ahk macros.',
    repository: 'https://github.com/SosnaPL/AutoHotkey-Macros',
  }
];

export default class Projects extends React.Component {
  public render(): JSX.Element {
    return (
      <>
        <Navbar active="projects" />
        <Row>
          <Col className="d-flex justify-content-center">
            <h2>Projects</h2>
          </Col>
        </Row>
        <div className="projects_container">
          <Row>
            <Col>
              <Container>
                {projects.map((project) => (
                  <Row key={project.title}>
                    <Col className="d-flex justify-content-start pb-3">
                      <Project project={project} />
                    </Col>
                  </Row>
                ))}
              </Container>
            </Col>
          </Row>
        </div>
        <Footer />
      </>
    );
  }
}
