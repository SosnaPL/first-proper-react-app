import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Navbar } from '../components/navbar';
import Project from '../components/project';
import { Footer } from '../components/footer';

const private_projects = [
  {
    title: 'Nostale-DMG-Calculator',
    description: 'Dmg calculator for Nostale made with python that includes information about hits/misses/deads/maxhit for all players on your current map.',
  }
];

export default class PrivateProjects extends React.Component {
  public render(): JSX.Element {
    return (
      <>
        <Navbar active="private_projects" />
        <Row>
          <Col className="d-flex justify-content-center">
            <h2>Private Projects</h2>
          </Col>
        </Row>
        <Row>
          <Col>
            <Container>
              {private_projects.map((project) => (
                <Row key={project.title}>
                  <Col className="d-flex justify-content-start pb-3">
                    <Project project={project} />
                  </Col>
                </Row>
              ))}
            </Container>
          </Col>
        </Row>
        <Footer />
      </>
    );
  }
}
